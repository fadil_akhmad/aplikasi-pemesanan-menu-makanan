import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class formmakan extends JFrame{
    private JPanel pane;
    private JTextField textField1;
    private JTextField textField2;
    public JComboBox comboBox1;
    private JButton pesanButton;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JButton bayarButton;

    public formmakan(){
        comboBox1.addItem("Es teh - Rp. 3.000,00");
        comboBox1.addItem("Es Jeruk - Rp. 3.000,00");
        comboBox1.addItem("Nasi Ayam - Rp. 12.000,00");
        comboBox1.addItem("Nasi Goreng - Rp. 12.000,00");
        comboBox1.addItem("Nasi Goreng Seafood - Rp. 15.000,00");
        comboBox1.addItem("Ayam Goreng - Rp. 15.000,00");
    }

    public formmakan (String judul){
        super(judul);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(new formmakan().pane);
        this.pack();
        pesanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nama = textField1.getText();
                String tanggalPesan = textField2.getText();
                String menu = String.valueOf(comboBox1);
                textField3.setText(nama);
                textField4.setText(tanggalPesan);
                textField5.setText(menu);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new formmakan("Rumah makan fadhil");
        frame.setVisible(true);
    }

}
